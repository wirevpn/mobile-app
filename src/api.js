import AsyncStorage from '@react-native-community/async-storage';
import g from 'WireVPN/src/globals.js';
import fetch from 'WireVPN/src/fetch.js';

const www = __DEV__ && !g.BYPASS ? 'https://testapp.wirevpn.net/v3' : 'https://app.wirevpn.net/v3';

export default class WireAPI {
	login(accountID, deviceID, pubKey) {
		return this.POST('/sessions', {
			account_id: accountID,
			device_id: deviceID, 	// We won't need this SOON™
			public_key: pubKey		// Cause we don't actually care about devices but pubKeys
		});
	}

	users() {
		return this.GET('/users');
	}

	plans() {
		return this.GET('/plans');
	}

	cancelPlan() {
		return this.DELETE('/plans');
	}

	countries() {
		return this.GET('/countries');
	}

	keys(pubKey) {
		return this.POST('/keys', {public_key: pubKey});
	}

	connect(country) {
		return this.POST('/connections', {country_code: country});
	}

	disconnect() {
		return this.DELETE('/connections');
	}

	async GET(uri) {
		// We take it from storage everytime because of Android's silly unpredictable life cycles
		// Just in case when JWT_TOKEN is refreshed via /keys
		let token = await AsyncStorage.getItem('JWT_TOKEN');
		return fetch(www + uri, {
			method: 'GET',
			headers: {
				'Accept': 'application/json',
				'Authorization': 'Bearer ' + token
			}
		}).then(resp => resp.json());
	}

	async POST(uri, obj) {
		// We take it from storage everytime because of Android's silly unpredictable life cycles
		// Just in case when JWT_TOKEN is refreshed via /keys
		let token = await AsyncStorage.getItem('JWT_TOKEN');
		return fetch(www + uri, {
			method: 'POST',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + token
			},
			body: JSON.stringify(obj)
		}).then(resp => resp.json());
	}

	async DELETE(uri) {
		// We take it from storage everytime because of Android's silly unpredictable life cycles
		// Just in case when JWT_TOKEN is refreshed via /keys
		let token = await AsyncStorage.getItem('JWT_TOKEN');
		return fetch(www + uri, {
			method: 'DELETE',
			headers: {
				'Accept': 'application/json',
				'Authorization': 'Bearer ' + token
			}
		}).then(resp => resp.json());
	}
}