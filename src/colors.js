const Colors = {
	white:		'#ece9e6',
	whiteLight: '#fdfaf7',
	whiteGhost: '#ece9e699',
	black:		'#4b6478',
	blackTrans: '#00000066',
	blackGhost: '#4b647833',
	blackDark:  '#000000',
	blueDark:	'#1e3c72',
	blueLight:	'#2a5298',
	blueToast:	'#8bb7f0',
	greenToast: '#99c99e',
	grayLight:	'lightgray',
};

export default Colors;