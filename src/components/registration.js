import React from 'react';
import changeNavigationBarColor from 'react-native-navigation-bar-color';
import c from 'WireVPN/src/colors.js';
import { WebView } from 'react-native-webview';
import SpinnerOverlay from 'WireVPN/src/modules/spinner.js';
import Button from 'WireVPN/src/modules/button.js';
import Toast from 'WireVPN/src/modules/toast.js';
import g from 'WireVPN/src/globals.js';
import AsyncStorage from '@react-native-community/async-storage';
import { Platform, StyleSheet } from 'react-native';
import { Body, Container, Header, Icon, Left, Right, Title, Content } from 'native-base';

export default class Registration extends React.Component {
	constructor(props) {
		super(props);
		this.uri        = __DEV__ && !g.BYPASS ? 'https://testapp.wirevpn.net/v2/create?did=' : 'https://app.wirevpn.net/v2/create?did=';
		this.onReceive	= this.onReceive.bind(this);
		this.toast		= this.toast.bind(this);
		this.js         = `
		var acn = document.getElementById("acn");
		if (acn != null) {
			window.ReactNativeWebView.postMessage(acn.value);
		}`;
		this.state		= {
			spinner: false
		};

		// Just in case. Some modules reset it
		if (Platform.OS !== 'ios') {
			changeNavigationBarColor(c.white, true);
		}
	}

	componentDidMount() {
		AsyncStorage.getItem('DEVICE_ID').then(did => {
			if (did == null) {
				this.setState({did: ''});
			} else {
				this.setState({did: did});
			}
		});
	}

	onReceive(event) {
		AsyncStorage.setItem('REGISTERED', 'true');
		this.props.navigation.state.params.onGoBack(event.nativeEvent.data);
		this.props.navigation.goBack(null);
	}

	toast(msg) {
		this.setState({spinner: false});
		this.refs.toast.show(msg);
	}

	render() {
		return (
			<Container style = {styles.screenContainer}>
				<Header iosBarStyle = 'light-content' androidStatusBarColor = {c.blueDark} style = {styles.header}>
					<Left>
						<Button transparent
							event = 'CLICK_BACK_REGISTER'
							hitSlop = {{top: 40, bottom: 40, left: 40, right: 40}}
							onPress = {() => this.props.navigation.goBack(null)}>
							<Icon name = 'arrow-back' style = {styles.backIcon}/>
						</Button>
					</Left>
					<Body>
						<Title style = {styles.headerTitle}>Create Account</Title>
					</Body>
					<Right />
				</Header>
				<Content contentContainerStyle = {styles.content}>
					<WebView
						ref = "webviewbridge"
						style = {styles.webBg}
						injectedJavaScript = {this.js}
						useWebKit = {true}
						onError = {error => this.toast(error.message)}
						onLoadStart = {() => this.setState({spinner: true})}
						onLoadEnd = {() => this.setState({spinner: false})}
						onMessage = {this.onReceive}
						scrollEnabled = {false}
						bounces = {false}
						cacheEnabled = {false}
						source = {{uri: this.uri + this.state.did}}/>
					<SpinnerOverlay color = {c.white} visible = {this.state.spinner}/>
				</Content>
				<Toast ref = 'toast'/>
			</Container>
		);
	}
}

const styles = StyleSheet.create({
	screenContainer: {
		backgroundColor: c.white
	},
	content: {
		flex: 1
	},
	header: {
		borderBottomWidth: 0,
		backgroundColor: c.blueDark,
	},
	headerTitle: {
		color: c.white
	},
	backIcon: {
		color: c.white
	},
	webBg: {
		backgroundColor: c.blueDark
	}
});