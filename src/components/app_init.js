import React from 'react';
import changeNavigationBarColor from 'react-native-navigation-bar-color';
import c from 'WireVPN/src/colors.js';
import { Spinner } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import { Platform, View, StatusBar, Image, StyleSheet } from 'react-native';

export default class AppInit extends React.Component {
	constructor(props) {
		super(props);

		// Just in case. Some modules reset it
		if (Platform.OS !== 'ios') {
			changeNavigationBarColor(c.white, true);
		}
	}

	componentDidMount() {
		AsyncStorage.getItem('JWT_TOKEN')
			.then(token => {
				this.setState({spinner: false});
				this.props.navigation.navigate(token ? 'Main' : 'Sign');
			});
	}

	render() {
		return (
			<View style = {styles.screenContainer}>
				<StatusBar
					animated = {false}
					barStyle = 'light-content'
					backgroundColor = {c.blueDark}
					translucent = {false}/>
				<View style = {styles.logoContainer}>
					<Image source = {logoImg} style = {styles.logo}/>
				</View>
				<View style = {styles.spinnerContainer}>
					<Spinner style = {styles.spinner} color = {c.white}/>
				</View>
			</View>
		);
	}
}

const logoImg = require('WireVPN/src/img/logo.png');

const styles = StyleSheet.create({
	screenContainer: {
		flex: 1,
		backgroundColor: c.blueDark
	},
	logoContainer: {
		flex: 1,
		flexDirection:'row',
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: c.blueDark
	},
	logo: {
		flex: 0.6,
		resizeMode: 'contain'
	},
	spinnerContainer: {
		flex: 1,
		alignItems: 'center'
	},
	spinner: {
		flex: 1
	}
});