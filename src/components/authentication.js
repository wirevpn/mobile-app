import React from 'react';
import changeNavigationBarColor from 'react-native-navigation-bar-color';
import x25519 from 'react-native-x25519-keys';
import UUIDGenerator from 'react-native-uuid-generator';
import SpinnerOverlay from 'WireVPN/src/modules/spinner.js';
import Button from 'WireVPN/src/modules/button.js';
import Toast from 'WireVPN/src/modules/toast.js';
import WireAPI from 'WireVPN/src/api.js';
import c from 'WireVPN/src/colors.js';
import { Icon, Item, Input, Text } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import { Platform, KeyboardAvoidingView, View, StatusBar, Image, StyleSheet,
	Vibration } from 'react-native';

export default class Authentication extends React.Component {
	constructor(props) {
		super(props);
		this.login 		= this.login.bind(this);
		this.onGoBack 	= this.onGoBack.bind(this);
		this.toast		= this.toast.bind(this);
		this.api		= new WireAPI();
		this.state 		= {
			spinner: false,
			accountID: ''
		};

		// Just in case. Some modules reset it
		if (Platform.OS !== 'ios') {
			changeNavigationBarColor(c.white, true);
		}
	}

	componentDidMount() {
		// Check account pass
		AsyncStorage.getItem('ACCOUNT_PASS').then(pass => {
			if (pass != null) {
				this.setState({accountID: pass});
			}
		});

		// Check device id
		AsyncStorage.getItem('DEVICE_ID').then(did => {
			if (did != null) {
				this.setState({deviceID: did});
			} else {
				UUIDGenerator.getRandomUUID().then(uuid => this.setState({deviceID: uuid}));
			}
		});
	}

	toast(msg) {
		this.setState({spinner: false});
		this.refs.toast.show(msg);
	}

	login() {
		// Empty acount shouldn't trigger a login
		if(!this.state.accountID) {
			return;
		}
		this.setState({spinner: true});

		// Prepate new keys (every login generates a new key)
		x25519.GenKeyPair()
			.then(keys => {
				this.api.login(this.state.accountID, this.state.deviceID, keys.public_key)
					.then(resp => {
						if (resp.status === 200) {
							AsyncStorage.setItem('DEVICE_ID', this.state.deviceID);
							AsyncStorage.setItem('ACCOUNT_PASS', this.state.accountID);
							AsyncStorage.setItem('JWT_TOKEN', resp.data.jwt_token);
							AsyncStorage.setItem('KEYS', JSON.stringify(keys));
							AsyncStorage.getItem('REGISTERED').then(resp => {
								this.setState({spinner: false});
								this.props.navigation.navigate(resp === 'true' ? 'Sign1' : 'Main');
							});
						} else {
							this.toast(resp.message);
						}
					})
					.catch(error => this.toast(error.message));
			})
			.catch(error =>  this.toast(error.message));
	}

	// This is for QR and register screen to call when the Account Pass is acquired
	// Triggers an automatic login
	onGoBack(data) {
		this.setState({accountID: data}, this.login);
		Vibration.vibrate(500);
	}

	render() {
		return (
			<View style = {styles.screenContainer}>
				<StatusBar
					animated = {false}
					barStyle = 'light-content'
					backgroundColor = {c.blueDark}
					translucent = {false}/>
				<View style = {styles.logoContainer}>
					<Image source = {logoImg} style = {styles.logo}/>
				</View>
				<KeyboardAvoidingView behavior = {Platform.OS === 'ios' ? 'padding' : null} style = {styles.formContainer}>
					<View style = {styles.inputContainer}>
						<Item underline style = {styles.inputItem}>
							<Input
								placeholder = 'Account Pass'
								placeholderTextColor = {c.white}
								style = {styles.input}
								onChangeText = {text => this.setState({accountID: text})}
								value = {this.state.accountID}/>
							<Button transparent
								event = 'CLICK_QR'
								onPress = {() => this.props.navigation.navigate('QR', {
									onGoBack: data => this.onGoBack(data)})}>
								<Icon name = 'qr-scanner' style = {styles.qrIcon}/>
							</Button>
						</Item>
					</View>
					<View style = {styles.loginButtonContainer}>
						<Button block bordered light rounded
							event = 'CLICK_SIGN_IN'
							style = {styles.loginButton}
							onPress = {this.login}>
							<Text style = {styles.loginButtonText}>Sign In</Text>
						</Button>
					</View>
					<View style = {styles.registerButtonContainer}>
						<Button block light rounded
							event = 'CLICK_REGISTER'
							style = {styles.registerButton}
							onPress = {() => this.props.navigation.navigate('Register', {
								onGoBack: data => this.onGoBack(data)})}>
							<Text style = {styles.registerButtonText}>New Account</Text>
						</Button>
					</View>
				</KeyboardAvoidingView>
				<SpinnerOverlay color = {c.white} visible = {this.state.spinner}/>
				<Toast ref = 'toast'/>
			</View>
		);
	}
}

const logoImg = require('WireVPN/src/img/logo.png');

const styles = StyleSheet.create({
	screenContainer: {
		flex: 1,
		backgroundColor: c.blueDark
	},
	logoContainer: {
		flex: 0.9,
		flexDirection:'row',
		justifyContent: 'center',
		alignItems: 'center'
	},
	logo: {
		flex: 0.6,
		resizeMode: 'contain'
	},
	formContainer: {
		flex: 1,
		justifyContent: 'center'
	},
	inputContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
		marginTop: 60,
		marginBottom: 30
	},
	inputItem: {
		flex: 0.8
	},
	input: {
		color: c.white
	},
	qrIcon: {
		color: c.white
	},
	loginButtonContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
		marginBottom: 30
	},
	loginButton: {
		flex: 0.8,
		height: 50,
		elevation: 0,
		borderColor: c.white
	},
	loginButtonText: {
		color: c.white
	},
	registerButtonContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
		marginBottom: 30
	},
	registerButton: {
		flex: 0.8,
		height: 50,
		elevation: 0,
		backgroundColor: c.white
	},
	registerButtonText: {
		color: c.blueLight
	}
});