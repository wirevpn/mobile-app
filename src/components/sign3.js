import React from 'react';
import changeNavigationBarColor from 'react-native-navigation-bar-color';
import Button from 'WireVPN/src/modules/button.js';
import c from 'WireVPN/src/colors.js';
import { Container, Content, Text, H1 } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import { Platform, View, StatusBar, StyleSheet } from 'react-native';

export default class Sign1 extends React.Component {
	constructor(props) {
		super(props);

		// Just in case. Some modules reset it
		if (Platform.OS !== 'ios') {
			changeNavigationBarColor(c.white, true);
		}
	}

	render() {
		return (
			<Container style = {styles.screenContainer}>
				<Content contentContainerStyle = {styles.content}>
					<StatusBar
						animated = {false}
						barStyle = 'light-content'
						backgroundColor = {c.blueDark}
						translucent = {false}/>
					<View style = {styles.titleContainer}>
						<H1 style = {styles.title}>Help Us Improve</H1>
					</View>
					<View style = {styles.descContainer}>
						<Text style = {styles.description}>
							By authorizing us to collect anonymous usage data like click events,
							page views and crash reports, you can helps us design a better user experience.
							You can always turn this feature on or off from your account settings.
							<Text style = {styles.helpEmphasize}> Do you authorize?</Text>
						</Text>
					</View>
					<View style = {styles.helpButtonContainer}>
						<View>
						</View>
						<View style = {styles.buttonContainer}>
							<View style = {styles.buttonSizeContainer}>
								<Button block bordered light rounded
									event = 'CLICK_NO_SIGN3'
									onPress = {() => {
										AsyncStorage.removeItem('ANALYTICS');
										AsyncStorage.removeItem('REGISTERED');
										this.props.navigation.navigate('Main');}}>
									<Text>No</Text>
								</Button>
							</View>
							<View style = {styles.buttonSizeContainer}>
								<Button block light rounded
									event = 'CLICK_YES_SIGN3'
									onPress = {() => {
										AsyncStorage.removeItem('REGISTERED');
										AsyncStorage.setItem('ANALYTICS', 'true');
										this.props.navigation.navigate('Main');}}>
									<Text>Yes</Text>
								</Button>
							</View>
						</View>
					</View>
				</Content>
			</Container>
		);
	}
}

const styles = StyleSheet.create({
	screenContainer: {
		flex: 1,
		backgroundColor: c.blueDark
	},
	content: {
		flex: 1,
		padding: 20,
	},
	titleContainer: {
		flex: 0.2,
		alignItems: 'center',
		justifyContent: 'center'
	},
	title: {
		color: c.white
	},
	descContainer: {
		flex: 0.2,
		marginBottom: 50
	},
	description: {
		lineHeight: 26,
		color: c.whiteGhost
	},
	helpEmphasize: {
		fontWeight: 'bold',
		color: c.white
	},
	helpButtonContainer: {
		flex: 0.7,
		justifyContent: 'space-between'
	},
	buttonContainer: {
		justifyContent: 'center',
		flexDirection: 'row',
		marginBottom: 10
	},
	buttonSizeContainer: {
		flex: 0.6,
		margin: 5
	}
});