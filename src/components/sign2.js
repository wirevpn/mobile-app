import React from 'react';
import changeNavigationBarColor from 'react-native-navigation-bar-color';
import Touchable from 'WireVPN/src/modules/touchable.js';
import Button from 'WireVPN/src/modules/button.js';
import c from 'WireVPN/src/colors.js';
import { Container, Content, Text, H1 } from 'native-base';
import { Linking, Platform, View, StatusBar, StyleSheet } from 'react-native';

export default class Sign2 extends React.Component {
	constructor(props) {
		super(props);

		// Just in case. Some modules reset it
		if (Platform.OS !== 'ios') {
			changeNavigationBarColor(c.white, true);
		}
	}

	render() {
		return (
			<Container style = {styles.screenContainer}>
				<Content contentContainerStyle = {styles.content}>
					<StatusBar
						animated = {false}
						barStyle = 'light-content'
						backgroundColor = {c.blueDark}
						translucent = {false}/>
					<View style = {styles.titleContainer}>
						<H1 style = {styles.title}>Terms of Service</H1>
					</View>
					<View style = {styles.descContainer}>
						<Text style = {styles.description}>
							By continuing you agree to our
							<Text style = {styles.termsEmphasize}> Terms</Text> and that you have read our
							<Text style = {styles.termsEmphasize}> Privacy Policy</Text>.
						</Text>
						<View style = {styles.readContainer}>
							<Touchable
								event = 'CLICK_READ_TOS'
								onPress = {() => Linking.openURL('http://help.wirevpn.net/support/solutions/articles/43000411092-wirevpn-terms-of-service')}>
								<Text style = {styles.readTerms}><Text style = {styles.termsEmphasize}>·</Text> Read terms of service</Text>
							</Touchable>
							<Touchable
								event = 'CLICK_READ_PRIVACY'
								onPress = {() => Linking.openURL('http://help.wirevpn.net/support/solutions/articles/43000411163-wirevpn-privacy-policy')}>
								<Text style = {styles.readTerms}><Text style = {styles.termsEmphasize}>·</Text> Read privacy policy</Text>
							</Touchable>
						</View>
					</View>
					<View style = {styles.termsButtonContainer}>
						<View>
						</View>
						<View style = {styles.buttonContainer}>
							<View style = {styles.buttonSizeContainer}>
								<Button block bordered light rounded
									event = 'CLICK_NEXT_SIGN2'
									onPress = {() => this.props.navigation.navigate('Sign3')}>
									<Text>Next</Text>
								</Button>
							</View>
						</View>
					</View>
				</Content>
			</Container>
		);
	}
}

const styles = StyleSheet.create({
	screenContainer: {
		flex: 1,
		backgroundColor: c.blueDark
	},
	content: {
		flex: 1,
		padding: 20,
	},
	titleContainer: {
		flex: 0.2,
		alignItems: 'center',
		justifyContent: 'center'
	},
	title: {
		color: c.white
	},
	descContainer: {
		flex: 0.2,
		marginBottom: 50
	},
	description: {
		lineHeight: 26,
		color: c.whiteGhost
	},
	readContainer: {
		marginTop: 15,
	},
	termsEmphasize: {
		fontWeight: 'bold',
		color: c.white
	},
	termsButtonContainer: {
		flex: 0.7,
		justifyContent: 'space-between'
	},
	readTerms: {
		color: c.white,
		lineHeight: 26,
	},
	buttonContainer: {
		justifyContent: 'center',
		flexDirection: 'row',
		marginBottom: 15
	},
	buttonSizeContainer: {
		flex: 0.6
	}
});