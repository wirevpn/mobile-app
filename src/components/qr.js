import React from 'react';
import {RNCamera} from 'react-native-camera';
import c from 'WireVPN/src/colors.js';
import Button from 'WireVPN/src/modules/button.js';
import changeNavigationBarColor from 'react-native-navigation-bar-color';
import { Platform, StyleSheet, View, Image } from 'react-native';
import { Header, Title, Left, Container, Body, Icon, Right } from 'native-base';

export default class QR extends React.Component {
	constructor(props) {
		super(props);
		this.onScan = this.onScan.bind(this);
		this.state = {
			render: false
		};

		// Just in case. Some modules reset it
		if (Platform.OS !== 'ios') {
			changeNavigationBarColor(c.white, true);
		}
	}

	componentDidMount() {
		// Wait before opening camera. Hack to prevent animation slowness
		setTimeout(() => this.setState({render: true}), 500);
	}

	onScan(data) {
		this.props.navigation.state.params.onGoBack(data);
		this.props.navigation.goBack(null);
	}

	render() {
		return (
			<Container style = {styles.screenContainer}>
				<Header iosBarStyle = 'light-content' androidStatusBarColor = {c.blueDark} style = {styles.header}>
					<Left>
						<Button transparent
							event = 'CLICK_BACK_QR'
							hitSlop = {{top: 40, bottom: 40, left: 40, right: 40}}
							onPress = {() => this.props.navigation.goBack(null)}>
							<Icon name = 'arrow-back' style = {styles.backIcon}/>
						</Button>
					</Left>
					<Body>
						<Title style = {styles.headerTitle}>Scan Account Pass</Title>
					</Body>
					<Right />
				</Header>
				{this.state.render
					? (
						<View style = {styles.cameraContainer}>
							<RNCamera
								style = {styles.cameraPreview}
								type = {RNCamera.Constants.Type.back}
								captureAudio = {false}
								androidCameraPermissionOptions = {{
									title: 'Permission to use camera',
									message: 'We need your permission to use your phone\'s camera',
									buttonPositive: 'Ok',
									buttonNegative: 'Cancel',
								}}
								onBarCodeRead = {e => this.onScan(e.data)}/>
							<View style = {styles.frameContainer}>
								<View style = {styles.frameTop}/>
								<View style = {styles.frameMid}>
									<View style = {styles.frameMidLeft}/>
									<View style = {styles.frameFrame}>
										<View style = {styles.cornerContainer}>
											<Image source = {topLeftImg} style = {styles.corner}/>
											<Image source = {topRightImg} style = {styles.corner}/>
										</View>
										<View style = {styles.cornerContainer}>
											<Image source = {bottomLeftImg} style = {styles.corner}/>
											<Image source = {bottomRightImg} style = {styles.corner}/>
										</View>
									</View>
									<View style = {styles.frameMidRight}/>
								</View>
								<View style = {styles.frameBottom}/>
							</View>
						</View>)
					: (
						<View style = {styles.empty}>
						</View>
					)}
			</Container>
		);
	}
}

const topLeftImg = require('WireVPN/src/img/top-left.png');
const topRightImg = require('WireVPN/src/img/top-right.png');
const bottomLeftImg = require('WireVPN/src/img/bottom-left.png');
const bottomRightImg = require('WireVPN/src/img/bottom-right.png');

const styles = StyleSheet.create({
	screenContainer: {
		backgroundColor: c.white
	},
	header: {
		borderBottomWidth: 0,
		backgroundColor: c.blueDark
	},
	backIcon: {
		color: c.white
	},
	headerTitle: {
		color: c.white
	},
	cameraPreview: {
		flex: 1,
		justifyContent: 'flex-end',
		alignItems: 'center'
	},
	cameraContainer: {
		flex: 1
	},
	frameContainer: {
		position: 'absolute',
		top: 0,
		left: 0,
		bottom: 0,
		right: 0,
	},
	frameTop: {
		flex: 0.3,
		backgroundColor: c.blackTrans
	},
	frameMid: {
		flex: 0.4,
		flexDirection: 'row'
	},
	frameBottom: {
		flex: 0.3,
		backgroundColor: c.blackTrans
	},
	frameMidLeft: {
		flex: 0.1,
		backgroundColor: c.blackTrans
	},
	frameMidRight: {
		flex: 0.1,
		backgroundColor: c.blackTrans
	},
	frameFrame: {
		flex: 0.8,
		justifyContent: 'space-between'
	},
	cornerContainer: {
		flexDirection: 'row',
		justifyContent: 'space-between'
	},
	corner: {
		resizeMode: 'stretch',
		width: 64,
		height: 64
	},
	empty: {
		flex: 1,
		backgroundColor: c.blackDark
	}
});