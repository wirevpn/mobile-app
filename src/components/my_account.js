import React from 'react';
import QRCode from 'react-native-qrcode-svg';
import changeNavigationBarColor from 'react-native-navigation-bar-color';
import WireGuard from 'react-native-wireguard';
import x25519 from 'react-native-x25519-keys';
import ActionSheet from 'react-native-action-sheet';
import Touchable from 'WireVPN/src/modules/touchable.js';
import ListItem from 'WireVPN/src/modules/listItem.js';
import SpinnerOverlay from 'WireVPN/src/modules/spinner.js';
import Button from 'WireVPN/src/modules/button.js';
import Toast from 'WireVPN/src/modules/toast.js';
import c from 'WireVPN/src/colors.js';
import WireAPI from 'WireVPN/src/api.js';
import AsyncStorage from '@react-native-community/async-storage';
import { Platform, StyleSheet, Clipboard, Alert } from 'react-native';
import { Header, Title, Left, Container, Content, Body, Icon, Right, CardItem, Card, Text,
	List, Switch } from 'native-base';

export default class MyAccount extends React.Component {
	constructor(props) {
		super(props);
		this.getUserAndPlans	= this.getUserAndPlans.bind(this);
		this.getPlans			= this.getPlans.bind(this);
		this.showPlansActions	= this.showPlansActions.bind(this);
		this.toast				= this.toast.bind(this);
		this.copyAccountID		= this.copyAccountID.bind(this);
		this.setSetting			= this.setSetting.bind(this);
		this.regenKeys			= this.regenKeys.bind(this);
		this.api                = new WireAPI();
		this.state				= {
			spinner: true,
			user: {
				account_id: 'loading',
				pay_id: '',
				days_left: 0
			},
			plans: {},
			planActions: [],
		};

		// Just in case. Some modules reset it
		if (Platform.OS !== 'ios') {
			changeNavigationBarColor(c.white, true);
		}
	}

	componentDidMount() {
		this.getUserAndPlans();

		// Get user info with timeout when there is a subscription
		// This gives our webhook server time to process its data
		this.props.navigation.addListener('didFocus', () => {
			if(this.state.sub == 'true') {
				this.setState({spinner: true});
				setTimeout(() => {
					this.setState({sub: null});
					this.getUserAndPlans();
					this.toast('Subscription succeeded', true);
				}, 5000);
			} else if(this.state.sub == 'false'){
				this.setState({sub: null});
				this.toast('Subscription failed');
			}
		});

		// Get switch settings
		AsyncStorage.getItem('KEEPALIVE').then(res => {
			if (res != null) {
				this.setState({keepalive: res === 'true' ? true : false});
			} else {
				this.setState({keepalive: false});
			}
		});

		AsyncStorage.getItem('ANALYTICS').then(res => {
			if (res != null) {
				this.setState({analytics: res === 'true' ? true : false});
			} else {
				this.setState({analytics: false});
			}
		});
	}

	regenKeys() {
		Alert.alert(
			'Disconnect',
			'Re-generating keys will cause current connection to be terminated. Are you sure?',
			[
				{text: 'No'},
				{text: 'Yes', onPress: () => {
					WireGuard.Disconnect();
					this.api.disconnect();
					x25519.GenKeyPair()
						.then(keys => {
							this.api.keys(keys.public_key)
								.then(resp => {
									if (resp.status === 200) {
										AsyncStorage.setItem('JWT_TOKEN', resp.data.jwt_token);
										AsyncStorage.setItem('KEYS', JSON.stringify(keys));
										this.toast('Keys regenerated', true);
									} else {
										this.toast(resp.message);
									}
								})
								.catch(error => this.toast(error.message));
						})
						.catch(error =>  this.toast(error.message));
				}},
			]
		);
	}

	setSetting(name, value) {
		if (name === 'ANALYTICS') {
			AsyncStorage.setItem(name, value == true ? 'true' : 'false').then(() => this.setState({analytics: value}));
		} else if (name === 'KEEPALIVE') {
			AsyncStorage.setItem(name, value == true ? 'true' : 'false').then(() => this.setState({keepalive: value}));
		}
	}

	getUserAndPlans() {
		this.setState({spinner: true});
		this.api.users()
			.then(resp => {
				if (resp.status === 200) {
					// Initiate plans info after the user info
					this.setState({
						spinner: false,
						user: resp.data
					}, this.getPlans);
				} else {
					this.toast(resp.message);
				}
			})
			.catch(error => this.toast(error.message));
	}

	getPlans() {
		this.api.plans()
			.then(resp => {
				if (resp.status === 200) {
					let pl = {};
					let pact = [];
					if (this.state.user.plan) {
						pact = ['Cancel'];
					} else {
						for(let i = 0; i < resp.data.length; i++) {
							let p = resp.data[i];
							pact.push(p.name);
							pl[p.name] = {
								passthrough: this.state.user.pay_id,
								product: p.product_id,
								coupon: p.coupon
							};
						}
					}
					this.setState({
						spinner: false,
						plans: pl,
						planActions: pact
					});
				} else {
					this.toast(resp.message);
				}
			})
			.catch(error => this.toast(error.message));
	}

	copyAccountID() {
		Clipboard.setString(this.state.user.account_id);
		this.toast('Copied', true);
	}

	showPlansActions() {
		ActionSheet.showActionSheetWithOptions({
			options: this.state.planActions,
			title: 'Subscription Plans'
		},
		index => {
			if (index >= 0) {
				if (this.state.planActions[index] == 'Cancel') {
					Alert.alert(
						'Cancel Plan',
						'Are you sure? You will still be able to use your days left after cancellation.',
						[
							{text: 'No'},
							{text: 'Yes', onPress: () => {
								this.setState({spinner: true});
								this.api.cancelPlan()
									.then(resp => {
										if (resp.status === 200) {
											setTimeout(() => {
												this.getUserAndPlans();
												this.toast('Subscription cancelled', true);
											}, 3000);
										} else {
											this.toast(resp.message);
										}
									})
									.catch(error => this.toast(error.message));
							}},
						]
					);
				} else {
					this.props.navigation.navigate('Subs', {
						subsInfo: this.state.plans[this.state.planActions[index]],
						onGoBack: data => this.setState({sub: data})
					});
				}
			}
		});
	}

	toast(msg, info) {
		this.setState({spinner: false});
		if (info) {
			this.refs.toast.show(msg, 1500, c.greenToast, c.black);
		} else {
			this.refs.toast.show(msg);
		}
	}

	render() {
		return (
			<Container style = {styles.screenContainer}>
				<Header iosBarStyle = 'light-content' androidStatusBarColor = {c.blueDark} style = {styles.header}>
					<Left>
						<Button transparent
							event = 'CLICK_DRAWER_MENU'
							hitSlop = {{top: 40, bottom: 40, left: 40, right: 40}}
							onPress = {() => this.props.navigation.openDrawer()}>
							<Icon name = 'menu' style = {styles.menuIcon}/>
						</Button>
					</Left>
					<Body>
						<Title style = {styles.headerTitle}>My Account</Title>
					</Body>
					<Right>
						<Button transparent
							event = 'CLICK_REFRESH_MY_ACCOUNT'
							hitSlop = {{top: 40, bottom: 40, left: 40, right: 40}}
							onPress = {this.getUserAndPlans}>
							<Icon name = 'refresh' style = {styles.refreshIcon}/>
						</Button>
					</Right>
				</Header>
				{!this.state.spinner
					? <Content contentContainerStyle = {styles.content}>
						<Card style = {styles.card}>
							<CardItem style = {styles.cardTitleContainer}>
								<Left>
									<Body>
										<Text style = {styles.cardTitle}>Account Pass</Text>
										<Text note>Keep your Account Pass safe and don't share it with anybody. Click to copy.</Text>
										<Touchable
											event = 'CLICK_COPY_ACCOUNT_PASS_INFO'
											onPress = {() => this.copyAccountID()}>
											<Text
												style = {styles.cardTextAccount}>
												{this.state.user.account_id}
											</Text>
										</Touchable>
									</Body>
								</Left>
							</CardItem>
							<CardItem style = {styles.cardQR}>
								<QRCode	backgroundColor = {c.whiteLight} size = {160} value = {this.state.user.account_id}/>
							</CardItem>
						</Card>
						<List>
							<ListItem>
								<Left style = {styles.cardLeft}>
									<Body>
										<Text style = {styles.cardTitle}>Days Left</Text>
										<Text note>Your account will be suspended after this duration unless you have a subscription.</Text>
									</Body>
								</Left>
								<Right>
									<Text style = {styles.cardTextRight}>{this.state.user.days_left}</Text>
								</Right>
							</ListItem>
							<ListItem
								event = 'CLICK_SHOW_PLANS'
								onPress = {this.showPlansActions}>
								<Left style = {styles.cardLeft}>
									<Body>
										<Text style = {styles.cardTitle}>Subscription</Text>
										<Text note>Current plan you're subscribed to. Click to change or cancel.</Text>
									</Body>
								</Left>
								<Right>
									<Text style = {styles.cardTextRight}>{this.state.user.plan ? this.state.user.plan : 'None'}</Text>
								</Right>
							</ListItem>
							<ListItem
								event = 'CLICK_REGEN_KEYS'
								onPress = {this.regenKeys}>
								<Left style = {styles.cardLeft}>
									<Body>
										<Text style = {styles.cardTitle}>Crypto Keys</Text>
										<Text note>Regenerate your encryption keys if you think they are exposed to third parties.</Text>
									</Body>
								</Left>
								<Right>
									<Icon name = 'refresh' style = {styles.cryptoIcon}/>
								</Right>
							</ListItem>
							<ListItem
								event = 'CLICK_TOGGLE_KEEPALIVE'
								onPress = {() => this.setSetting('KEEPALIVE', !this.state.keepalive)}>
								<Left style = {styles.cardLeft}>
									<Body>
										<Text style = {styles.cardTitle}>Keepalive</Text>
										<Text note>Only enable if you're behind a NAT firewall.</Text>
									</Body>
								</Left>
								<Right>
									<Switch value = {this.state.keepalive} onValueChange = {value => this.setSetting('KEEPALIVE', value)}/>
								</Right>
							</ListItem>
							<ListItem
								event = 'CLICK_TOGGLE_ANALYTICS'
								onPress = {() => this.setSetting('ANALYTICS', !this.state.analytics)}>
								<Left style = {styles.cardLeft}>
									<Body>
										<Text style = {styles.cardTitle}>Help Us</Text>
										<Text note>By enabling this you allow us to get anonymous information about crashes and UI events.</Text>
									</Body>
								</Left>
								<Right>
									<Switch value = {this.state.analytics} onValueChange = {value => this.setSetting('ANALYTICS', value)}/>
								</Right>
							</ListItem>
						</List>
					</Content>
					: null}
				<SpinnerOverlay color = {c.black} visible = {this.state.spinner}/>
				<Toast ref = 'toast'/>
			</Container>
		);
	}
}

const styles = StyleSheet.create({
	screenContainer: {
		backgroundColor: c.white
	},
	content: {
		padding: 16
	},
	header: {
		backgroundColor: c.blueDark,
	},
	headerTitle: {
		color: c.white
	},
	menuIcon: {
		color: c.white
	},
	refreshIcon: {
		color: c.white
	},
	cardLeft: {
		alignItems: 'center'
	},
	cardTitle: {
		color: c.black,
		fontWeight: 'normal'
	},
	cardTextRight: {
		color: c.black
	},
	cardTextAccount: {
		color: c.black,
		fontSize: 15,
		marginTop: 5
	},
	cardQR: {
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: c.whiteLight
	},
	cardTitleContainer: {
		backgroundColor: c.whiteLight
	},
	card: {
		backgroundColor: c.whiteLight,
		paddingBottom: 16,
	},
	cryptoIcon: {
		color: c.black
	}
});