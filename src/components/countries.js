import React from 'react';
import changeNavigationBarColor from 'react-native-navigation-bar-color';
import Touchable from 'WireVPN/src/modules/touchable.js';
import SignalIcon from 'WireVPN/src/modules/signal.js';
import FlagIcon from 'WireVPN/src/modules/flag.js';
import SpinnerOverlay from 'WireVPN/src/modules/spinner.js';
import Button from 'WireVPN/src/modules/button.js';
import Toast from 'WireVPN/src/modules/toast.js';
import c from 'WireVPN/src/colors.js';
import WireAPI from 'WireVPN/src/api.js';
import { Platform, View, StyleSheet, RefreshControl } from 'react-native';
import { Container, Header, Title, Content, Left, Right, Body, Icon, Text, } from 'native-base';

export default class Countries extends React.Component {
	constructor(props) {
		super(props);
		this.createList = this.createList.bind(this);
		this.onSelect   = this.onSelect.bind(this);
		this.onRefresh  = this.onRefresh.bind(this);
		this.getList    = this.getList.bind(this);
		this.toast      = this.toast.bind(this);
		this.api		= new WireAPI();
		this.state      = {
			refreshing: false,
			spinner: true,
			countryList: []
		};

		// Just in case. Some modules reset it
		if (Platform.OS !== 'ios') {
			changeNavigationBarColor(c.white, true);
		}
	}

	componentDidMount() {
		this.getList();
	}

	onRefresh(type) {
		if (type) {
			this.setState({spinner: true});
			this.getList();
		} else {
			this.setState({refreshing: true});
			this.getList();
		}
	}

	onSelect(data) {
		this.props.navigation.state.params.onSelect(data);
		this.props.navigation.goBack();
	}

	getList() {
		this.api.countries()
			.then(resp => {
				if (resp.status === 200) {
					this.setState({
						refreshing: false,
						spinner: false,
						countryList: resp.data
					});
				} else {
					this.toast(resp.message);
				}
			})
			.catch(error => this.toast(error.message));
	}

	toast(msg) {
		this.setState({
			refreshing: false,
			spinner: false
		});
		this.refs.toast.show(msg);
	}

	createList() {
		let table = [];
		let auto = {
			code: 'AUTO',
			name: 'Auto Select Country',
			strength: 5
		};
		for(let i = 0; i < this.state.countryList.length; i++) {
			let country = this.state.countryList[i];
			let item = (
				<Touchable
					event = {'CLICK_COUNTRY_' + country.code.toUpperCase() + '_SELECTED'}
					key = {country.name}
					onPress = {() => this.onSelect(country)}>
					<View style = {styles.countryContainer}>
						<View style = {styles.countryContainerLeft}>
							<FlagIcon country = {country.code} size = {32} type = 'office'/>
							<Text style = {styles.countryText}>{country.name}</Text>
						</View>
						<SignalIcon strength = {country.strength} size = {32} style = {styles.countrySignal}/>
					</View>
				</Touchable>
			);
			table.push(item);
		}
		table.push((
			<Touchable
				event = 'CLICK_COUNTRY_AUTO_SELECTED'
				key = {auto.name}
				onPress = {() => this.onSelect(auto)}>
				<View style = {styles.countryContainer}>
					<View style = {styles.countryContainerLeft}>
						<FlagIcon country = {auto.code} size = {32} type = 'office'/>
						<Text style = {styles.countryText}>{auto.name}</Text>
					</View>
					<SignalIcon strength = {auto.strength} size = {32} style = {styles.countrySignal}/>
				</View>
			</Touchable>
		));
		return table;
	}

	render() {
		return (
			<Container style = {styles.screenContainer}>
				<Header iosBarStyle = 'light-content' androidStatusBarColor = {c.blueDark} style = {styles.header}>
					<Left>
						<Button transparent
							event = 'CLICK_DRAWER_MENU'
							hitSlop = {{top: 40, bottom: 40, left: 40, right: 40}}
							onPress = {() => this.props.navigation.goBack()}>
							<Icon name = 'arrow-back' style = {styles.menuIcon}/>
						</Button>
					</Left>
					<Body>
						<Title style = {styles.headerTitle}>Choose Location</Title>
					</Body>
					<Right>
						<Button transparent
							event = 'CLICK_REFRESH_COUNTRIES'
							hitSlop = {{top: 40, bottom: 40, left: 40, right: 40}}
							onPress = {() => this.onRefresh(true)}>
							<Icon name = 'refresh' style = {styles.refreshIcon}/>
						</Button>
					</Right>
				</Header>
				<Content
					style = {styles.content}
					contentContainerStyle = {{flexGrow: 1}}
					refreshControl = {<RefreshControl
						refreshing = {this.state.refreshing}
						onRefresh = {this.onRefresh.bind(this)}/>}>
					{this.state.spinner ? null : this.createList()}
					<SpinnerOverlay color = {c.black} visible = {this.state.spinner}/>
				</Content>
				<Toast ref = 'toast'/>
			</Container>
		);
	}
}

const styles = StyleSheet.create({
	screenContainer: {
		backgroundColor: c.white,
	},
	header: {
		backgroundColor: c.blueDark,
	},
	headerTitle: {
		color: c.white
	},
	menuIcon: {
		color: c.white
	},
	refreshIcon: {
		color: c.white
	},
	content: {
		backgroundColor: c.white,
	},
	countryContainer: {
		padding: 9,
		borderBottomWidth: 0.5,
		borderColor: c.grayLight,
		height: 64,
		flexDirection:'row',
		alignItems: 'center',
		justifyContent: 'space-between'
	},
	countryContainerLeft: {
		paddingLeft: 6,
		flexDirection: 'row',
		alignItems: 'center'
	},
	countryText: {
		paddingLeft: 20,
		color: c.black
	},
	countrySignal: {
		paddingRight: 6
	}
});