import React from 'react';
import changeNavigationBarColor from 'react-native-navigation-bar-color';
import Touchable from 'WireVPN/src/modules/touchable.js';
import Button from 'WireVPN/src/modules/button.js';
import c from 'WireVPN/src/colors.js';
import Toast from 'WireVPN/src/modules/toast.js';
import { Container, Content, Text, H1 } from 'native-base';
import AsyncStorage from '@react-native-community/async-storage';
import { Platform, View, StatusBar, StyleSheet, Clipboard } from 'react-native';

export default class Sign1 extends React.Component {
	constructor(props) {
		super(props);
		this.copyAccountID = this.copyAccountID.bind(this);
		this.state = {
			accountID: ''
		};

		// Just in case. Some modules reset it
		if (Platform.OS !== 'ios') {
			changeNavigationBarColor(c.white, true);
		}
	}

	componentDidMount() {
		AsyncStorage.getItem('ACCOUNT_PASS').then((id) => this.setState({accountID: id}));
	}

	copyAccountID() {
		Clipboard.setString(this.state.accountID);
		this.refs.toast.show('Copied');
	}

	render() {
		return (
			<Container style = {styles.screenContainer}>
				<Content contentContainerStyle = {styles.content}>
					<StatusBar
						animated = {false}
						barStyle = 'light-content'
						backgroundColor = {c.blueDark}
						translucent = {false}/>
					<View style = {styles.titleContainer}>
						<H1 style = {styles.title}>Congratulations</H1>
					</View>
					<View style = {styles.descContainer}>
						<Text style = {styles.description}>
							You have been successfully registered.
							Please <Text style = {styles.passEmphasize}>don't lose your Account Pass</Text>.
							As we don't require e-mail for registration,
							recovering a lost account pass can only be done via a support ticket.
						</Text>
					</View>
					<View style = {styles.passButtonContainer}>
						<View style = {styles.passContainer}>
							<Text style = {styles.accountPassTitle}>Your Account Pass</Text>
							<Touchable
								event = 'CLICK_COPY_ACCOUNT_PASS_SIGN1'
								onPress = {this.copyAccountID}>
								<Text style = {styles.accountPass}>{this.state.accountID}</Text>
							</Touchable>
						</View>
						<View style = {styles.buttonContainer}>
							<View style = {styles.buttonSizeContainer}>
								<Button block bordered light rounded
									event = 'CLICK_NEXT_SIGN1'
									onPress = {() => this.props.navigation.navigate('Sign2')}>
									<Text>Next</Text>
								</Button>
							</View>
						</View>
					</View>
				</Content>
				<Toast ref = 'toast' bgColor = {c.greenToast} textColor = {c.black}/>
			</Container>
		);
	}
}

const styles = StyleSheet.create({
	screenContainer: {
		flex: 1,
		backgroundColor: c.blueDark
	},
	content: {
		flex: 1,
		padding: 20,
	},
	titleContainer: {
		flex: 0.2,
		alignItems: 'center',
		justifyContent: 'center'
	},
	title: {
		color: c.white
	},
	descContainer: {
		flex: 0.2,
		marginBottom: 50
	},
	description: {
		lineHeight: 26,
		color: c.whiteGhost
	},
	passEmphasize: {
		fontWeight: 'bold',
		color: c.white
	},
	passButtonContainer: {
		flex: 0.6,
		justifyContent: 'space-between'
	},
	passContainer: {
		alignItems: 'center'
	},
	accountPassTitle: {
		fontFamily: Platform.OS === 'ios' ? 'Courier New' : 'monospace',
		color: c.whiteGhost,
		marginBottom: 10
	},
	accountPass: {
		fontSize: 30,
		fontFamily: Platform.OS === 'ios' ? 'Courier New' : 'monospace',
		color: c.white
	},
	buttonContainer: {
		justifyContent: 'center',
		flexDirection: 'row',
		marginBottom: 15
	},
	buttonSizeContainer: {
		flex: 0.6
	}
});