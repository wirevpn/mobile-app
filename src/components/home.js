import React from 'react';
import LinearGradient from 'react-native-linear-gradient';
import changeNavigationBarColor from 'react-native-navigation-bar-color';
import Touchable from 'WireVPN/src/modules/touchable.js';
import base64 from 'react-native-base64';
import SignalIcon from 'WireVPN/src/modules/signal.js';
import FlagIcon from 'WireVPN/src/modules/flag.js';
import SpinnerOverlay from 'WireVPN/src/modules/spinner.js';
import Button from 'WireVPN/src/modules/button.js';
import Toast from 'WireVPN/src/modules/toast.js';
import WireAPI from 'WireVPN/src/api.js';
import c from 'WireVPN/src/colors.js';
import firebase from 'react-native-firebase';
import WireGuard from 'react-native-wireguard';
import AsyncStorage from '@react-native-community/async-storage';
import { Icon, Text, Title, Left, Body, Right, Container, Header } from 'native-base';
import { Platform, View, Image, StyleSheet, Dimensions, TouchableOpacity,
	DeviceEventEmitter } from 'react-native';

export default class Home extends React.Component {
	constructor(props) {
		super(props);
		this.api                = new WireAPI();
		this.getNotif			= this.getNotif.bind(this);
		this.connect			= this.connect.bind(this);
		this.onCountrySelect	= this.onCountrySelect.bind(this);
		this.toast				= this.toast.bind(this);
		this.regularListener	= this.regularListener.bind(this);
		this.exceptionListener	= this.exceptionListener.bind(this);
		this.establish			= this.establish.bind(this);
		this.disconnected 		= this.disconnected.bind(this);
		this.connected			= this.connected.bind(this);
		this.onHostResumed		= this.onHostResumed.bind(this);
		this.onHostDestroyed	= this.onHostDestroyed.bind(this);
		this.state				= {
			spinner: false,
			connection: false,
			notification: false,
			paused: false,
			notifText: '',
			notifDays: 0,
			country: {
				name: 'Germany',
				code: 'DE',
				strength: 5
			},
			lastOnline: {}
		};

		// Just in case. Some modules reset it
		if (Platform.OS !== 'ios') {
			changeNavigationBarColor(c.white, true);
		}
	}

	componentDidMount() {
		// Check last online country in storage
		AsyncStorage.getItem('LAST_ONLINE').then(country => {
			if (country) {
				let c = JSON.parse(country);
				this.setState({country: c, lastOnline: c});
			} else {
				this.api.countries().then(resp => {
					if (resp.status === 200 && resp.data.length > 0) {
						this.setState({country: resp.data[0]});
					}
				});
			}
		});

		this.notifListener = this.props.navigation.addListener('didFocus', this.getNotif);
		console.log('REGISTERED_NOTIF_LISTENER');

		this.expEmit = DeviceEventEmitter.addListener(WireGuard.EV_TYPE_EXCEPTION, this.exceptionListener);
		console.log('REGISTERED_EXCEPTION_EVENT_LISTENER');

		this.regEmit = DeviceEventEmitter.addListener(WireGuard.EV_TYPE_REGULAR, this.regularListener);
		console.log('REGISTERED_REGULAR_EVENT_LISTENER');

		// Needs to be called here as well because onHostResume is not called when app is killed
		this.onHostResumed();

		// Check if we're allowed to collect anonymous data
		AsyncStorage.getItem('ANALYTICS').then(resp => {
			if (resp === 'true') {
				firebase.analytics().setAnalyticsCollectionEnabled(true);
			} else {
				firebase.analytics().setAnalyticsCollectionEnabled(false);
			}
		});
	}

	componentWillUnmount() {
		this.expEmit && this.expEmit.remove();
		this.regEmit && this.regEmit.remove();
		console.log('UNREGISTERED_ALL_EVENT_LISTENERS');

		// Get notification and listen to it every focus
		this.notifListener && this.notifListener.remove();
		console.log('UNREGISTERED_NOTIF_LISTENER');
	}

	regularListener(e) {
		switch (e) {
		case WireGuard.EV_REVOKED:
			this.disconnected();
			break;
		case WireGuard.EV_HOST_DESTROYED:
			this.onHostDestroyed();
			break;
		case WireGuard.EV_HOST_RESUMED:
			this.onHostResumed();
			this.setState({paused: false});
			break;
		case WireGuard.EV_HOST_PAUSED:
			this.setState({paused: true});
			break;
		}
		console.log(e);
	}

	exceptionListener(e) {
		this.disconnected();
		this.toast('Unexpected WireGuard error');
		console.log(e);
	}

	disconnected() {
		this.api.disconnect().catch(error => this.toast(error.message));
		if (this.state.connection && this.state.country.code !== this.state.lastOnline.code) {
			// Trigger reconnect if another country selected
			this.connect(false);
		} else {
			this.setState({connection: false});
			this.toast('Disconnected', 'blue');
		}
	}

	connected() {
		this.setState({connection: true, lastOnline: this.state.country});
		AsyncStorage.setItem('LAST_ONLINE', JSON.stringify(this.state.country));
		this.toast('Connected', 'green');
	}

	onHostResumed() {
		// Just in case the app was cleaned let's sync the backend and frontend online status
		WireGuard.Status().then(status => {
			if (status && !this.state.connection) {
				this.setState({connection: true});
			} else if (status != null && (!status && this.state.connection)) {
				this.setState({connection: false});
			}
		});
	}

	onHostDestroyed() {
		// Some android version leave the notification icon there even when app is killed completely
		if (WireGuard.Status().then(resp => {
			if (!resp) {
				this.disconnected();
			}
		}));
	}

	getNotif() {
		this.api.users().then(resp => {
			if (resp.status === 200) {
				if (resp.data.days_left <= 7) {
					this.setState({
						notif: {
							text: resp.data.plan
								? 'You will be charged automatically'
								: 'Please subscribe to avoid service interruption',
							days: resp.data.days_left
						}
					});
				} else {
					this.setState({notif: null});
				}
			}
		});
		console.log('NOTIFICATION_CHECKED');
	}

	onCountrySelect(data) {
		this.setState({country: data});
	}

	connect(state) {
		this.setState({spinner: true});
		if (!state) {
			this.setState({lastOnline: this.state.country});
			this.api.connect(this.state.country.code)
				.then(resp => {
					if (resp.status === 200) {
						this.establish(resp.data);
					} else {
						this.toast(resp.message);
					}
				})
				.catch(error => this.toast(error.message));
		} else {
			WireGuard.Disconnect().then(resp => {
				if (resp) {
					this.disconnected();
					firebase.analytics().logEvent('CLICK_DISCONNECT_FROM_' + this.state.country.code);
				}
			});
		}
	}

	establish(config) {
		let c = base64.decode(config);
		AsyncStorage.getItem('KEYS').then(keys => {
			// We prepend the public key of the user
			let privKey = JSON.parse(keys).private_key;
			c = c.replace('[Interface]\n', '[Interface]\nPrivateKey=' + privKey + '\n');
			AsyncStorage.getItem('KEEPALIVE').then(keepalive => {
				if (keepalive) {
					// We append the keepalive if it's enabled
					c = c + '\n' + 'PersistentKeepalive=25\n';
				}
				WireGuard.Connect(c, this.state.country.name, {
					icon: 'wirevpn',
					title: 'WireVPN',
					text: this.state.country.code === 'AUTO'
						? 'Connected'
						: 'Connected to ' + this.state.country.name
				})
					.then(() => {
						this.connected();
						firebase.analytics().logEvent('CLICK_CONNECT_TO_' + this.state.country.code);
					})
					.catch(e => this.toast(e.message));
			});
		});
	}

	toast(msg, info) {
		this.setState({spinner: false});
		if (info == 'green') {
			this.refs.toast.show(msg, 1500, c.greenToast, c.black);
		} else if (info == 'blue') {
			this.refs.toast.show(msg, 1500, c.blueToast, c.white);
		} else {
			this.refs.toast.show(msg);
		}
	}

	render() {
		return (
			<Container style = {styles.screenContainer}>
				<LinearGradient colors = {[c.blueDark, c.blueLight]} style = {styles.gradient}>
					<Header iosBarStyle = 'light-content' androidStatusBarColor = {c.blueDark} style = {styles.header}>
						<Left style = {styles.headerLeft}>
							<Button transparent
								event = 'CLICK_DRAWER_MENU'
								hitSlop = {{top: 40, bottom: 40, left: 40, right: 40}}
								onPress = {() => this.props.navigation.openDrawer()}>
								<Icon name = 'menu' style = {styles.menuIcon}/>
							</Button>
						</Left>
						<Body style = {styles.headerBody}>
							<Title style = {styles.headerTitle}>WireVPN</Title>
						</Body>
						<Right />
					</Header>
					<View style = {styles.powerContainer}>
						<TouchableOpacity
							onPress = {() => this.connect(this.state.connection && this.state.country.code === this.state.lastOnline.code)}
							style = {styles.powerTouch}>
							{this.state.connection && this.state.country.code === this.state.lastOnline.code
								? <Image source = {activeImg}/>
								: <Image source = {inactiveImg}/>}
						</TouchableOpacity>
					</View>
					<Image source = {curveImg} fadeDuration = {0} style = {styles.curve}/>
				</LinearGradient>
				<View style = {styles.bottomContainer}>
					{this.state.notif
						? <View style = {styles.notifContainer}>
							<Touchable
								event = 'CLICK_NOTIF'
								onPress = {() => this.props.navigation.navigate('Account')}>
								<View style = {styles.notifFrame}>
									<Icon type = 'EvilIcons' name = 'exclamation' style = {styles.notifIcon}/>
									<View style = {styles.notifTextContainer}>
										{this.state.notif.days > 1
											? <Text style = {styles.notifText}>You have {this.state.notif.days} days left</Text>
											: <Text style = {styles.notifText}>You have {this.state.notif.days} day left</Text>}
										<Text style = {styles.notifText}>{this.state.notif.text}</Text>
									</View>
								</View>
							</Touchable>
						</View>
						: null}
					<Touchable
						event = 'CLICK_COUNTRY_SELECTION'
						onPress = {() => this.props.navigation.navigate('Country', {onSelect: data => this.onCountrySelect(data)})}>
						<View style = {styles.footerContainer}>
							<View style = {styles.footerLeft}>
								<FlagIcon country = {this.state.country.code} size = {32} type = 'office'/>
								<Text style = {styles.footerCountry}>{this.state.country.name}</Text>
							</View>
							<SignalIcon strength = {this.state.country.strength} size = {32} style = {styles.footerSignal}/>
						</View>
					</Touchable>
				</View>
				<SpinnerOverlay color = {c.white} visible = {this.state.spinner} paddingTop = {70}/>
				<Toast ref = 'toast'/>
			</Container>
		);
	}
}

const inactiveImg = require('WireVPN/src/img/power-inactive.png');
const activeImg = require('WireVPN/src/img/power-active.png');
const curveImg = require('WireVPN/src/img/curve.png');

const styles = StyleSheet.create({
	screenContainer: {
		flex: 1,
		backgroundColor: c.blueDark
	},
	gradient: {
		flex: 0.65,
		justifyContent: 'space-between'
	},
	header: {
		backgroundColor: 'transparent',
		shadowOpacity: 0,
		borderWidth: 0,
		borderBottomWidth: 0,
		elevation: 0
	},
	headerTitle: {
		color: c.white
	},
	headerLeft: {
		flex: 1
	},
	headerBody: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	menuIcon: {
		color: c.white,
	},
	powerContainer: {
		paddingTop: 40,
		alignItems: 'center',
		justifyContent: 'center',
		flexDirection: 'row'
	},
	powerTouch: {
		flex: 0.5,
		alignItems: 'center',
		justifyContent: 'center'
	},
	curve: {
		width: Dimensions.get('window').width,
		resizeMode: 'stretch'
	},
	bottomContainer: {
		flex: 0.35,
		backgroundColor: c.white,
		justifyContent: 'flex-end'
	},
	notifContainer: {
		flexDirection: 'row',
		justifyContent: 'center',
		marginBottom: 20
	},
	notifIcon: {
		color: c.black,
		paddingRight: 5
	},
	notifFrame: {
		flexDirection: 'row',
		borderRadius: 10,
		borderWidth: 1,
		borderColor: c.black,
		padding: 10,
		alignItems:'center'
	},
	notifTextContainer: {
		flexDirection: 'column',
		justifyContent: 'center'
	},
	notifText: {
		color: c.black,
		fontSize: 12,
		flexWrap: 'wrap'
	},
	footerContainer: {
		padding: 9,
		borderTopWidth: 0.5,
		borderBottomWidth: 0.5,
		borderColor: c.grayLight,
		height: 64,
		flexDirection:'row',
		alignItems: 'center',
		justifyContent: 'space-between'
	},
	footerLeft: {
		paddingLeft: 6,
		flexDirection: 'row',
		alignItems: 'center'
	},
	footerCountry: {
		paddingLeft: 20,
		color: c.black
	},
	footerSignal: {
		paddingRight: 6
	}
});