import React from 'react';
import changeNavigationBarColor from 'react-native-navigation-bar-color';
import WireGuard from 'react-native-wireguard';
import ListItem from 'WireVPN/src/modules/listItem.js';
import WireAPI from 'WireVPN/src/api.js';
import g from 'WireVPN/src/globals.js';
import c from 'WireVPN/src/colors.js';
import AsyncStorage from '@react-native-community/async-storage';
import { Linking, Platform, Image, StyleSheet, Alert } from 'react-native';
import { Text, Container, List, Content, Icon, Footer } from 'native-base';

export default class Sidebar extends React.Component {
	constructor(props) {
		super(props);
		this.api    = new WireAPI();
		this.state  = {
			wvVersion: '1.0.0',
			wgVersion: ''
		};
		this.routes = [
			{
				title: 'Home',
				icon: 'location',
				event: 'CLICK_MENU_HOME',
				onPress: () => {
					this.props.navigation.navigate('Connect');
					this.props.navigation.closeDrawer();
				}
			},
			{
				title: 'Account',
				icon: 'user',
				event: 'CLICK_MENU_INFO',
				onPress: () => {
					this.props.navigation.navigate('Account');
					this.props.navigation.closeDrawer();
				}

			},
			{
				title: 'Privacy Policy',
				icon: 'lock',
				event: 'CLICK_MENU_PRIVACY',
				onPress: () => {
					Linking.openURL('http://help.wirevpn.net/support/solutions/articles/43000411163-wirevpn-privacy-policy');
					this.props.navigation.closeDrawer();
				}
			},
			{
				title: 'Terms of Service',
				icon: 'envelope',
				event: 'CLICK_MENU_TOS',
				onPress: () => {
					Linking.openURL('http://help.wirevpn.net/support/solutions/articles/43000411092-wirevpn-terms-of-service');
					this.props.navigation.closeDrawer();
				}
			},
			{
				title: 'Sign Out',
				icon: 'close-o',
				event: 'CLICK_MENU_SIGN_OUT',
				onPress: () => {
					Alert.alert(
						'Sign Out',
						'Are you sure? Before signing out, please make sure to save your Account Pass.',
						[
							{text: 'No'},
							{text: 'Yes', onPress: () => {
								WireGuard.Disconnect();
								this.api.disconnect();
								AsyncStorage.removeItem('REGISTERED');
								AsyncStorage.removeItem('JWT_TOKEN');
								this.props.navigation.navigate('Sign');
							}},
						]
					);
				}
			}
		];

		// Just in case. Some modules reset it
		if (Platform.OS !== 'ios') {
			changeNavigationBarColor(c.white, true);
		}
	}

	componentDidMount() {
		WireGuard.Version().then((v) => this.setState({wgVersion: v}));
		this.setState({bannerImg: banners[Math.floor(Math.random()* 7)]});
	}

	render() {
		return (
			<Container>
				<Content style = {styles.content}>
					<Image source = {this.state.bannerImg} style = {styles.banner} />
					<List
						dataArray = {this.routes}
						contentContainerStyle = {styles.list}
						renderRow = {data => {
							return (
								<ListItem button
									event = {data.event}
									onPress = {data.onPress}>
									<Icon type = 'EvilIcons' name = {data.icon} style = {styles.listIcon}/>
									<Text style = {styles.listText}>{data.title}</Text>
								</ListItem>
							);
						}}/>
				</Content>
				<Footer style = {styles.footer}>
					<Text style = {styles.footerText}>WireVPN {g.WV_VERSION}</Text>
					<Text style = {styles.footerText}>WireGuard {this.state.wgVersion}</Text>
				</Footer>
			</Container>
		);
	}
}

const banners = [
	require('WireVPN/src/img/cover1.jpg'),
	require('WireVPN/src/img/cover2.jpg'),
	require('WireVPN/src/img/cover3.jpg'),
	require('WireVPN/src/img/cover4.jpg'),
	require('WireVPN/src/img/cover5.jpg'),
	require('WireVPN/src/img/cover6.jpg'),
	require('WireVPN/src/img/cover7.jpg'),
];

const styles = StyleSheet.create({
	banner: {
		height: 200,
		width: '100%',
		resizeMode: 'cover',
		justifyContent: 'center',
		alignItems: 'center'
	},
	content: {
		backgroundColor: c.white
	},
	list: {
		marginTop: 5
	},
	listText: {
		color: c.black
	},
	listIcon: {
		color: c.black,
		marginRight: 10
	},
	footer: {
		flexDirection: 'column',
		backgroundColor: c.white,
		paddingLeft: 26
	},
	footerText: {
		color: c.blackGhost,
		fontSize: 12
	}
});