import React from 'react';
import changeNavigationBarColor from 'react-native-navigation-bar-color';
import { WebView } from 'react-native-webview';
import SpinnerOverlay from 'WireVPN/src/modules/spinner.js';
import Button from 'WireVPN/src/modules/button.js';
import c from 'WireVPN/src/colors.js';
import g from 'WireVPN/src/globals.js';
import { Platform, StyleSheet } from 'react-native';
import { Body, Container, Header, Icon, Left, Right, Title, Content } from 'native-base';

export default class Subscription extends React.Component {
	constructor(props) {
		super(props);
		this.uri            = __DEV__ && !g.BYPASS ? 'https://testapp.wirevpn.net/v2/subs' : 'https://app.wirevpn.net/v2/subs';
		this.onSendReceive	= this.onSendReceive.bind(this);
		this.js             = `
		function openCheckout(opts) {
			opts.successCallback = function() {
				window.ReactNativeWebView.postMessage('true');
			};
			opts.closeCallback = function() {
				window.ReactNativeWebView.postMessage('false');
			};
			Paddle.Checkout.open(opts);
		}
		Paddle.Setup({vendor: 36395});
		openCheckout({{DATA}});`;
		this.state			= {
			spinner: false
		};

		// Just in case. Some modules reset it
		if (Platform.OS !== 'ios') {
			changeNavigationBarColor(c.white, true);
		}
	}

	onSendReceive(event) {
		this.props.navigation.state.params.onGoBack(event.nativeEvent.data);
		this.props.navigation.goBack(null);
	}

	render() {
		return (
			<Container style = {styles.screenContainer}>
				<Header iosBarStyle = 'light-content' androidStatusBarColor = {c.blueDark} style = {styles.header}>
					<Left>
						<Button transparent
							event = 'CLICK_BACK_SUBS'
							hitSlop = {{top: 40, bottom: 40, left: 40, right: 40}}
							onPress = {() => this.props.navigation.goBack(null)}>
							<Icon name = 'arrow-back' style = {styles.backIcon}/>
						</Button>
					</Left>
					<Body>
						<Title style = {styles.headerTitle}>Subscription</Title>
					</Body>
					<Right />
				</Header>
				<Content contentContainerStyle = {styles.content}>
					<WebView
						ref = "webview"
						style = {styles.webBg}
						injectedJavaScript = {
							this.js.replace(
								'{{DATA}}',
								JSON.stringify(this.props.navigation.getParam('subsInfo'))
							)}
						useWebKit = {true}
						onError = {error => this.toast(error.message)}
						onLoadStart = {() => this.setState({spinner: true})}
						onLoadEnd = {() => this.setState({spinner: false})}
						scrollEnabled = {false}
						bounces = {false}
						cacheEnabled = {false}
						scalesPageToFit = {false}
						onMessage = {this.onSendReceive}
						source = {{uri: this.uri}}/>
					<SpinnerOverlay color = {c.black} visible = {this.state.spinner}/>
				</Content>
			</Container>
		);
	}
}

const styles = StyleSheet.create({
	screenContainer: {
		backgroundColor: c.white
	},
	content: {
		flex: 1
	},
	header: {
		backgroundColor: c.blueDark,
	},
	headerTitle: {
		color: c.white
	},
	backIcon: {
		color: c.white
	},
	webBg: {
		backgroundColor: c.white
	}
});