import React from 'react';
import { ListItem } from 'native-base';
import firebase from 'react-native-firebase';

export default class TrackerTouchable extends React.Component {
	render() {
		return (
			<ListItem
				button = {this.props.button}
				onPress = {() => {
					this.props.event && firebase.analytics().logEvent(this.props.event);
					this.props.onPress && this.props.onPress();
				}}
				style = {this.props.style}>
				{this.props.children}
			</ListItem>
		);
	}
}