import React from 'react';
import Touchable from 'react-native-platform-touchable';
import firebase from 'react-native-firebase';

export default class TrackerTouchable extends React.Component {
	render() {
		return (
			<Touchable
				onPress = {() => {
					this.props.event && firebase.analytics().logEvent(this.props.event);
					this.props.onPress && this.props.onPress();
				}}
				hitSlop = {this.props.hitSlop}
				style = {this.props.style}>
				{this.props.children}
			</Touchable>
		);
	}
}