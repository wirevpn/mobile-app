import React from 'react';
import { Image } from 'react-native';

export default class SignalIcon extends React.Component {
	getImage(str) {
		let img = images[str];
		if (img == null) {
			return images['highest'];
		}
		return img;
	}

	render() {
		return (
			<Image
				style = {[{width: this.props.size, height: this.props.size}, this.props.style]}
				source = {this.getImage(this.props.strength)}/>
		);
	}
}

const images = {
	1: require('WireVPN/src/img/signal1.png'),
	2: require('WireVPN/src/img/signal2.png'),
	3: require('WireVPN/src/img/signal3.png'),
	4: require('WireVPN/src/img/signal4.png'),
	5: require('WireVPN/src/img/signal5.png')
};