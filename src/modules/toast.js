import React from 'react';
import c from 'WireVPN/src/colors.js';
import { Animated, Text, View, StyleSheet } from 'react-native';

export default class Toast extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			visible: false,
			fadeAnim: new Animated.Value(0),
			bgColor: c.black,
			textColor: c.white,
		};
	}

	show(msg, duration, bgColor, textColor) {
		duration = duration || 1500;
		let animation = 250;

		this.setState({
			message: msg || '',
			visible: true,
			bgColor: (bgColor || this.props.bgColor) || this.state.bgColor,
			textColor: (textColor || this.props.textColor) || this.state.textColor,
		},
		// Start fade in
		Animated.timing(this.state.fadeAnim, {
			toValue: 1,
			duration: animation,
		}).start());

		// Schedule fade out
		setTimeout(() =>
			Animated.timing(this.state.fadeAnim, {
				toValue: 0,
				duration: animation,
			}).start(() => this.setState({visible: false})), duration + animation);
	}

	render() {
		return (
			<View>
				{this.state.visible
					? <Animated.View style = {[{
						backgroundColor: this.state.bgColor,
						opacity: this.state.fadeAnim}, styles.animated]}>
						<Text style = {{color: this.state.textColor}}>{this.state.message}</Text>
					</Animated.View>
					: null}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	animated: {
		position: 'absolute',
		bottom: 0,
		left: 0,
		right: 0,
		height: 50,
		padding: 15
	}
});