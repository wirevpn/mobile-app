import React from 'react';
import { Button } from 'native-base';
import firebase from 'react-native-firebase';

export default class TrackerButton extends React.Component {
	render() {
		return (
			<Button
				transparent = {this.props.transparent}
				block = {this.props.block}
				bordered = {this.props.bordered}
				light = {this.props.light}
				rounded =  {this.props.rounded}
				onPress = {() => {
					this.props.event && firebase.analytics().logEvent(this.props.event);
					this.props.onPress && this.props.onPress();
				}}
				hitSlop = {this.props.hitSlop}
				style = {this.props.style}>
				{this.props.children}
			</Button>
		);
	}
}