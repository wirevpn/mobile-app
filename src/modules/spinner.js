import React from 'react';
import { View, Modal } from 'react-native';
import { Spinner } from 'native-base';

export default class SpinnerOverlay extends React.Component {
	render() {
		return (
			<Modal
				onRequestClose = {() => {}}
				transparent = {true}
				visible = {this.props.visible}>
				<View style = {{
					position: 'absolute',
					top: 0, bottom: 0, left: 0, right: 0,
					justifyContent: 'center',
					alignItems: 'center'}}>
					<Spinner
						color = {this.props.color}
						style = {{
							paddingTop: this.props.paddingTop,
							paddingBottom: this.props.paddingBottom,
							paddingLeft: this.props.paddingLeft,
							paddingRight: this.props.paddingRight
						}}/>
				</View>
			</Modal>
		);
	}
}