import React from 'react';
import { Image } from 'react-native';

export default class FlagIcon extends React.Component {
	getImage(country, type) {
		let img = images[type][country];
		if (img == null) {
			return images['empty'];
		}
		return img;
	}

	render() {
		return (
			<Image
				style = {[{width: this.props.size, height: this.props.size}, this.props.style]}
				source = {this.getImage(this.props.country, this.props.type)}/>
		);
	}
}

const images = {
	'empty': require('WireVPN/src/img/emptyFlag.png'),
	'office': {
		'AM': require('WireVPN/src/img/office/am.png'),
		'AR': require('WireVPN/src/img/office/ar.png'),
		'AT': require('WireVPN/src/img/office/at.png'),
		'AU': require('WireVPN/src/img/office/au.png'),
		'AUTO': require('WireVPN/src/img/office/auto.png'),
		'AZ': require('WireVPN/src/img/office/az.png'),
		'BA': require('WireVPN/src/img/office/ba.png'),
		'BB': require('WireVPN/src/img/office/bb.png'),
		'BE': require('WireVPN/src/img/office/be.png'),
		'BF': require('WireVPN/src/img/office/bf.png'),
		'BG': require('WireVPN/src/img/office/bg.png'),
		'BJ': require('WireVPN/src/img/office/bj.png'),
		'BO': require('WireVPN/src/img/office/bo.png'),
		'BR': require('WireVPN/src/img/office/br.png'),
		'BS': require('WireVPN/src/img/office/bs.png'),
		'BW': require('WireVPN/src/img/office/bw.png'),
		'BY': require('WireVPN/src/img/office/by.png'),
		'CA': require('WireVPN/src/img/office/ca.png'),
		'CG': require('WireVPN/src/img/office/cg.png'),
		'CH': require('WireVPN/src/img/office/ch.png'),
		'CL': require('WireVPN/src/img/office/cl.png'),
		'CM': require('WireVPN/src/img/office/cm.png'),
		'CN': require('WireVPN/src/img/office/cn.png'),
		'CO': require('WireVPN/src/img/office/co.png'),
		'CR': require('WireVPN/src/img/office/cr.png'),
		'CU': require('WireVPN/src/img/office/cu.png'),
		'CZ': require('WireVPN/src/img/office/cz.png'),
		'DE': require('WireVPN/src/img/office/de.png'),
		'DK': require('WireVPN/src/img/office/dk.png'),
		'DZ': require('WireVPN/src/img/office/dz.png'),
		'EE': require('WireVPN/src/img/office/ee.png'),
		'EG': require('WireVPN/src/img/office/eg.png'),
		'ES': require('WireVPN/src/img/office/es.png'),
		'FI': require('WireVPN/src/img/office/fi.png'),
		'FR': require('WireVPN/src/img/office/fr.png'),
		'GA': require('WireVPN/src/img/office/ga.png'),
		'GB': require('WireVPN/src/img/office/gb.png'),
		'GR': require('WireVPN/src/img/office/gr.png'),
		'HR': require('WireVPN/src/img/office/hr.png'),
		'HT': require('WireVPN/src/img/office/ht.png'),
		'HU': require('WireVPN/src/img/office/hu.png'),
		'IE': require('WireVPN/src/img/office/ie.png'),
		'IL': require('WireVPN/src/img/office/il.png'),
		'IN': require('WireVPN/src/img/office/in.png'),
		'IR': require('WireVPN/src/img/office/ir.png'),
		'IS': require('WireVPN/src/img/office/is.png'),
		'IT': require('WireVPN/src/img/office/it.png'),
		'JM': require('WireVPN/src/img/office/jm.png'),
		'JP': require('WireVPN/src/img/office/jp.png'),
		'KG': require('WireVPN/src/img/office/kg.png'),
		'KR': require('WireVPN/src/img/office/kr.png'),
		'KW': require('WireVPN/src/img/office/kw.png'),
		'KZ': require('WireVPN/src/img/office/kz.png'),
		'LR': require('WireVPN/src/img/office/lr.png'),
		'LT': require('WireVPN/src/img/office/lt.png'),
		'LU': require('WireVPN/src/img/office/lu.png'),
		'LV': require('WireVPN/src/img/office/lv.png'),
		'MA': require('WireVPN/src/img/office/ma.png'),
		'MC': require('WireVPN/src/img/office/mc.png'),
		'MK': require('WireVPN/src/img/office/mk.png'),
		'ML': require('WireVPN/src/img/office/ml.png'),
		'MT': require('WireVPN/src/img/office/mt.png'),
		'MX': require('WireVPN/src/img/office/mx.png'),
		'NL': require('WireVPN/src/img/office/nl.png'),
		'NO': require('WireVPN/src/img/office/no.png'),
		'NZ': require('WireVPN/src/img/office/nz.png'),
		'PA': require('WireVPN/src/img/office/pa.png'),
		'PE': require('WireVPN/src/img/office/pe.png'),
		'PH': require('WireVPN/src/img/office/ph.png'),
		'PL': require('WireVPN/src/img/office/pl.png'),
		'PR': require('WireVPN/src/img/office/pr.png'),
		'PT': require('WireVPN/src/img/office/pt.png'),
		'RO': require('WireVPN/src/img/office/ro.png'),
		'RS': require('WireVPN/src/img/office/rs.png'),
		'RU': require('WireVPN/src/img/office/ru.png'),
		'SA': require('WireVPN/src/img/office/sa.png'),
		'SC': require('WireVPN/src/img/office/sc.png'),
		'SE': require('WireVPN/src/img/office/se.png'),
		'SG': require('WireVPN/src/img/office/sg.png'),
		'SI': require('WireVPN/src/img/office/si.png'),
		'SK': require('WireVPN/src/img/office/sk.png'),
		'SM': require('WireVPN/src/img/office/sm.png'),
		'TD': require('WireVPN/src/img/office/td.png'),
		'TG': require('WireVPN/src/img/office/tg.png'),
		'TJ': require('WireVPN/src/img/office/tj.png'),
		'TN': require('WireVPN/src/img/office/tn.png'),
		'TR': require('WireVPN/src/img/office/tr.png'),
		'TW': require('WireVPN/src/img/office/tw.png'),
		'UA': require('WireVPN/src/img/office/ua.png'),
		'UK': require('WireVPN/src/img/office/uk.png'),
		'USA': require('WireVPN/src/img/office/usa.png'),
		'UY': require('WireVPN/src/img/office/uy.png'),
		'UZ': require('WireVPN/src/img/office/uz.png')
	}
};