import React from 'react'; // eslint-disable-line
import c from 'WireVPN/src/colors.js';
import AppInitScreen from 'WireVPN/src/components/app_init.js';
import AuthScreen from 'WireVPN/src/components/authentication.js';
import QRScreen from 'WireVPN/src/components/qr.js';
import RegisterScreen from 'WireVPN/src/components/registration.js';
import HomeScreen from 'WireVPN/src/components/home.js';
import CountryScreen from 'WireVPN/src/components/countries.js';
import SideBar from 'WireVPN/src/components/sidebar.js';
import AccountScreen from 'WireVPN/src/components/my_account.js';
import SubsScreen from 'WireVPN/src/components/subscription.js';
import Sign1Screen from 'WireVPN/src/components/sign1.js';
import Sign2Screen from 'WireVPN/src/components/sign2.js';
import Sign3Screen from 'WireVPN/src/components/sign3.js';
import firebase from 'react-native-firebase';
import { createSwitchNavigator,	createStackNavigator, createDrawerNavigator, createAppContainer } from 'react-navigation';

// Login/Register screen
const AuthStack = createStackNavigator({
	Auth: AuthScreen,
	QR: QRScreen,
	Register: RegisterScreen,
	Sign1: Sign1Screen,
	Sign2: Sign2Screen,
	Sign3: Sign3Screen,
},{
	initialRouteName: 'Auth',
	headerMode: 'none'
});

// Home screen
const HomeStack = createStackNavigator({
	Home: HomeScreen,
	Country: CountryScreen
},{
	initialRouteName: 'Home',
	headerMode: 'none'
});

// Account screen
const AccountStack = createStackNavigator({
	Info: AccountScreen,
	Subs: SubsScreen
},{
	initialRouteName: 'Info',
	headerMode: 'none'
});

// Drawer navigator
const HomeDrawer = createDrawerNavigator({
	Connect: HomeStack,
	Account: AccountStack
},{
	drawerBackgroundColor: c.white,
	initialRouteName: 'Connect',
	contentComponent: props => <SideBar {...props} />
});

const AppNavigator = createAppContainer(
	createSwitchNavigator({
		Init: AppInitScreen,
		Sign: AuthStack,
		Main: HomeDrawer
	},{
		initialRouteName: 'Init',
	})
);

export default () => (<AppNavigator
	onNavigationStateChange = {(prevState, currentState) => {
		const currentScreen = getActiveRouteName(currentState);
		const prevScreen = getActiveRouteName(prevState);
		if (prevScreen !== currentScreen) {
			firebase.analytics().setCurrentScreen('SCREEN_CHANGE_' + currentScreen.toUpperCase());
		}
	}}/>);

function getActiveRouteName(navigationState) {
	if (!navigationState) {
		return null;
	}
	const route = navigationState.routes[navigationState.index];
	if (route.routes) {
		return getActiveRouteName(route);
	}
	return route.routeName;
}
