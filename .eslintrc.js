module.exports = {
    "env": {
        "browser": false,
        "node": true,
        "react-native/react-native": true
    },
    "extends": "eslint:recommended",
    "parserOptions": {
        "ecmaFeatures": {
            "jsx": true
        },
        "ecmaVersion": 2018,
        "sourceType": "module"
    },
    "plugins": [
        "react",
        "react-native"
    ],
    "rules": {
        "no-console": "off",
        "indent": [
            "error",
            "tab"
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "quotes": [
            "error",
            "single"
        ],
        "semi": [
            "error",
            "always"
        ],
        "react-native/no-unused-styles": 2,
        "react-native/split-platform-components": 2,
        "react/jsx-uses-vars": 1,
        "react/jsx-no-undef": 1,
        "react/jsx-equals-spacing": [1, "always"],
    }
};