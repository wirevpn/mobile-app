package com.wirevpn.android;

import android.app.Application;

import com.facebook.react.ReactApplication;
import com.reactnativecommunity.asyncstorage.AsyncStoragePackage;
import com.reactnativecommunity.webview.RNCWebViewPackage;
import com.actionsheet.ActionSheetPackage;
import io.github.traviskn.rnuuidgenerator.RNUUIDGeneratorPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;
import com.facebook.soloader.SoLoader;
import com.wirevpn.x25519keys.RNX25519KeysPackage;
import com.wirevpn.rnwireguard.RNWireguardPackage;
import com.horcrux.svg.SvgPackage;
import com.thebylito.navigationbarcolor.NavigationBarColorPackage;
import com.BV.LinearGradient.LinearGradientPackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import org.reactnative.camera.RNCameraPackage;
import io.invertase.firebase.RNFirebasePackage;
import io.invertase.firebase.analytics.RNFirebaseAnalyticsPackage;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      return Arrays.<ReactPackage>asList(
          new MainReactPackage(),
            new AsyncStoragePackage(),
            new RNCWebViewPackage(),
            new ActionSheetPackage(),
            new RNUUIDGeneratorPackage(),
            new RNWireguardPackage(),
            new RNX25519KeysPackage(),
            new SvgPackage(),
            new NavigationBarColorPackage(),
            new LinearGradientPackage(),
            new RNGestureHandlerPackage(),
            new RNCameraPackage(),
            new RNFirebasePackage(),
            new RNFirebaseAnalyticsPackage()
        );
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);
  }
}
